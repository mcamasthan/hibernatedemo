package com.hibernate.manytomany;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;

import com.hibernate.util.HibernateUtil;



public class Test {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        // Create an employee
        Employee employee = new Employee();
        employee.setFirstName("Ashok");
        employee.setLastName("Kumar");
        
        Employee employee2 = new Employee();
        employee2.setFirstName("Masthan");
        employee2.setLastName("vali");

       // Create project1
        Project project = new Project();
        project.setTitle("Employee Management System");

        // Create project2
        Project project1 = new Project();
       project1.setTitle("Content Management System");
       
       Project project3 = new Project();
       project3.setTitle("CSC Management System");
       
       Project project4 = new Project();
       project4.setTitle("EEE Management System");

       // employee can work on two projects, Add project references in the employee
      // employee.getProjects().add(project);
      // employee.getProjects().add(project1);

        // Add employee reference in the projects
       //project.getEmployees().add(employee);
       //project1.getEmployees().add(employee);
       Set<Project> masthansetProjects=new HashSet();
       masthansetProjects.add(project3);
       masthansetProjects.add(project4);
       employee.setProjects(masthansetProjects);
       Set<Project> setProjects=new HashSet();
       setProjects.add(project1);
       setProjects.add(project);
       employee.setProjects(masthansetProjects);
       employee2.setProjects(setProjects);
       session.save(employee);
       session.save(employee2);
       session.getTransaction().commit();
       HibernateUtil.shutdown();
       
       
       
                 }
}