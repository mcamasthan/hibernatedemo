package com.hibernate.onetoone;


import com.hibernate.dao.InstructorDao;

public class ManApp {
    public static void main(String[] args) {

        Instructor instructor = new Instructor("Ashok", "Kumar", "ashok@gamil.com");
        InstructorDetail instructorDetail = new InstructorDetail("http://www.youtube.com", "Guitar");
        instructor.setInstructorDetail(instructorDetail);

        InstructorDao instructorDao = new InstructorDao();
        instructorDao.saveInstructor(instructor);
    }
}