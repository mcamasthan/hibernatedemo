package com.hibernate.onetomany;

import java.util.Arrays;

public class ManApp {
 public static void main(String[] args) {

  InstructorDao instructorDao = new InstructorDao();
  CourseDao courseDao = new CourseDao();
  
  Instructor instructor = new Instructor("Ashok", "Kumar", "ashok@gmail.com");

  Course tempCourse2 = new Course("The Btech Masterclass");
  tempCourse2.setInstructor(instructor);
  Course tempCourse1 = new Course("CSC - The Ultimate Guide");
  tempCourse1.setInstructor(instructor);
  instructor.setCourses(Arrays.asList(tempCourse2,tempCourse1));
  // create some courses
  
 // courseDao.saveCourse(tempCourse1);  
  
  
 // courseDao.saveCourse(tempCourse2);
  
  instructorDao.saveInstructor(instructor);
 // instructorDao.deleteInstructor(1);
  
  //tempCourse1.setInstructor(instructor);
 // courseDao.saveCourse(tempCourse1);
  
 }
}