package com.hibernate.app;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.entity.Employee;

public class NativeSqlDemo {

	private static SessionFactory factory; 
	   public static void main(String[] args) {
	      
	      try {
	         factory = new Configuration().configure().buildSessionFactory();
	      } catch (Throwable ex) { 
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); 
	      }
	      
	      NativeSqlDemo ME = new NativeSqlDemo();

	      /* Add few employee records in database */
	    /*  Integer empID1 = ME.addEmployee("Zara1", "Ali@gmail.com","abc" ,"IND",6000);
	      Integer empID2 = ME.addEmployee("Daisy1", "Das@gmail.com","bbc","SIN", 6000);
	      Integer empID3 = ME.addEmployee("John1", "Paul", "tbc","US",7000);
	      Integer empID4 = ME.addEmployee("Mohd1", "Yasee","uer","Japan", 1000);*/

	      /* List down employees and their salary using Scalar Query */
	     // ME.listEmployeesScalar();

	      /* List down complete employees information using Entity Query */
	      ME.listEmployeesEntity();
	   }
	   
	   /* Method to CREATE an employee in the database */
	   public Integer addEmployee(String fname, String email,String password,String country, int salary){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      Integer employeeID = null;
	      
	      try {
	         tx = session.beginTransaction();
	         Employee employee = new Employee(fname, email,password,country, salary);
	         employeeID = (Integer) session.save(employee); 
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	      return employeeID;
	   }

	   /* Method to  READ all the employees using Scalar Query */
	   public void listEmployeesScalar( ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         String sql = "SELECT name, salary FROM emp";// name=raja,salary=200
	         SQLQuery query = session.createSQLQuery(sql);
	         query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
	         List data = query.list();

	         for(Object object : data) {
	            Map row = (Map)object;
	            System.out.print("First Name: " + row.get("name")); 
	            System.out.println(", Salary: " + row.get("salary")); 
	         }
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }

	   /* Method to READ all the employees using Entity Query */
	   public void listEmployeesEntity( ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      
	      try {
	         tx = session.beginTransaction();
	         String sql = "SELECT * FROM emp";
	         SQLQuery query = session.createSQLQuery(sql);
	         query.addEntity(Employee.class);
	         List employees = query.list();

	         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
	            Employee employee = (Employee) iterator.next(); 
	            System.out.print("First Name: " + employee.getName()); 
	            System.out.print(" Email: " + employee.getEmail()); 
	            System.out.println("  Salary: " + employee.getSalary()); 
	         }
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      } finally {
	         session.close(); 
	      }
	   }
}
