package com.hibernate.app;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.entity.Employee;
import com.hibernate.util.HibernateUtil;

public class HqlDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Transaction transaction = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
       // update ****************************
       /* Query q=session.createQuery("update Customer set name=:name where id=:id"); // :name,:id named parameters 
        q.setParameter("name","Ashok Kumar");  
        q.setParameter("id",1);  
          
        int status=q.executeUpdate();  
        System.out.println(status);
        
        transaction.commit();*/
        
       /* Query query=session.createQuery("select sum(salary) from Employee");  
        List<Integer> list=((org.hibernate.query.Query) query).list();  
        System.out.println(list.get(0));*/  
		
		
		/*String hql = "DELETE FROM Employee WHERE id = :employee_id";
	Query query = session.createQuery(hql);
	query.setParameter("employee_id", 4);
	int result = query.executeUpdate();
	System.out.println("Rows affected: " + result);
	transaction.commit();*/
	
	/*String hql = "FROM Employee E WHERE E.id > 2 ORDER BY E.salary DESC";
	Query query = session.createQuery(hql);
	List<Employee> results = ((org.hibernate.query.Query) query).list();
	System.out.println(results.size());*/
        
        String hql = "FROM Customer";
        Query query = session.createQuery(hql);
        query.setFirstResult(1);// initial count 
        query.setMaxResults(3);// max count 
        List results =  ((org.hibernate.query.Query) query).list();
        
        System.out.println("the result size is:"+results.size());

	}

}
