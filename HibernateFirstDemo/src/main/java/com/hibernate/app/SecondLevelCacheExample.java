package com.hibernate.app;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.entity.Employee;
import com.hibernate.util.HibernateUtil;

public class SecondLevelCacheExample {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Transaction transaction = null;
		//Session session = HibernateUtil.getSessionFactory().openSession();
		
		    Session session1=HibernateUtil.getSessionFactory().openSession();
		    Employee emp1=(Employee)session1.load(Employee.class,1);    
		    System.out.println(emp1.getId()+" "+emp1.getName()+" "+emp1.getSalary());    
		    session1.close();    
		        
		    Session session2=HibernateUtil.getSessionFactory().openSession();
		    Employee emp2=(Employee)session2.load(Employee.class,1);    
		    System.out.println(emp2.getId()+" "+emp2.getName()+" "+emp2.getSalary());    
		    session2.close();  
		
	}

}
