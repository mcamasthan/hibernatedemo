package com.hibernate.app;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.entity.Customer;
import com.hibernate.util.HibernateUtil;

public class HibenateDemo {
	
	public static void main(String[] args) {
		   Transaction transaction = null;
		/*Customer c=new Customer();
		c.setName("Test2");
		c.setFirstName("TestDemo1");
		c.setLastName("last");
		c.setEmail("test1@gmail.com");
		c.setPhone("1234");
		c.setPassword("abc1");
		c.setAddress("Banglore");
      //  Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the Customer object
            session.save(c);
           // session.save(c);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        */
      /*  Session session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Customer cust=  session.get(Customer.class, 18);
        System.out.println(cust.getLastName());
        cust.setLastName("Kumar");
        session.update(cust);
        transaction.commit();
        Customer cust1=  session.get(Customer.class, 18);
        System.out.println(cust1.getLastName());*/
        
      //  cust.setFirstName("avc");
        /*try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List < Customer > customers = session.createQuery("from Customer", Customer.class).list();//HQL
            customers.forEach(cust->{System.out.println(cust.getFirstName());});
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        */
		   
		   Session session = HibernateUtil.getSessionFactory().openSession();
		   transaction = session.beginTransaction();// ACID
		   Customer cust=  session.get(Customer.class, 18);
		 //  Customer cust1=  session.load(Customer.class, 18);
		   session.delete(cust);
		   transaction.commit();
        
    }

}
