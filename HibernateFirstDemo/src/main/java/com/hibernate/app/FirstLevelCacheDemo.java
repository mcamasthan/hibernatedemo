package com.hibernate.app;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.entity.Customer;
import com.hibernate.util.HibernateUtil;



public class FirstLevelCacheDemo {

    public static void main(String[] args) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            // start the transaction
            transaction = session.beginTransaction();

            // get the student entity using id
            Customer student1 = session.load(Customer.class, 1);// checks in session ,2 sessionfactory, 3 databse

            System.out.println(student1.getFirstName());
            System.out.println(student1.getLastName());
            System.out.println(student1.getEmail());
            session.evict(student1);
            // load student entity by id
            Customer student2 = session.load(Customer.class, 1);// checks in session 
            System.out.println(student2.getFirstName());
            System.out.println(student2.getLastName());
            System.out.println(student2.getEmail());
          
            // commit transaction
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}